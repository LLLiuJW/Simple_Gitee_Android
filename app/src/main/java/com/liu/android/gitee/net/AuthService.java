package com.liu.android.gitee.net;

import com.liu.android.gitee.bean.TokenEntity;

import io.reactivex.Observable;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.Headers;
import retrofit2.http.POST;

/**
 * 封装获取认证的请求
 */
public interface AuthService {

    //根据邮箱和密码获得accessToken
    @FormUrlEncoded
    @POST("/oauth/token")
    Observable<TokenEntity> getToken(@Field("grant_type") String grant_type,
                                     @Field("username") String username,
                                     @Field("password") String password,
                                     @Field("client_id") String client_id,
                                     @Field("client_secret") String client_secret,
                                     @Field("scope") String scope);

    //根据过期token的refresh_token重新获得token
    @FormUrlEncoded
    @POST("/oauth/token")
    Observable<TokenEntity> refreshToken(@Field("grant_type") String grant_type,
                                     @Field("refresh_token") String refresh_token);
}
