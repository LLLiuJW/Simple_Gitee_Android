package com.liu.android.gitee.ui.main;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.liu.android.gitee.R;
import com.liu.android.gitee.app.Constant;
import com.liu.android.gitee.base.BaseFragment;
import com.liu.android.gitee.databinding.FragmentAboutBinding;
import com.liu.android.gitee.ui.follow.ProfileDetailActivity;
import com.liu.android.gitee.ui.repos.ReposDetailActivity;
import com.liu.android.gitee.utils.CommonUtil;


/**
 * 关于页面
 */
public class AboutFragment extends BaseFragment {

    private FragmentAboutBinding binding;

    @Override
    protected View initContentView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        binding = FragmentAboutBinding.inflate(inflater, container, false);
        return binding.getRoot();
    }

    @Override
    protected void initListener() {
        binding.llSourcecode.setOnClickListener(v -> {
            startActivity(ReposDetailActivity.getStartIntent(mContext, Constant.APP_REPOS_NAME, getString(R.string.authorName)));
        });
        binding.llProfile.setOnClickListener(v -> {
            startActivity(ProfileDetailActivity.getStartIntent(mContext, getString(R.string.authorName)));
        });
    }

    @Override
    protected void initData() {
        binding.tvVersion.setText(CommonUtil.getVersion());
    }

}