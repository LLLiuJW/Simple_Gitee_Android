package com.liu.android.gitee.net;

/**
 * 用于网络请求的接口回调
 */
public interface HttpListener {
    void onSuccess(Object object);

    void onError(String errorMsg, String code);
}
