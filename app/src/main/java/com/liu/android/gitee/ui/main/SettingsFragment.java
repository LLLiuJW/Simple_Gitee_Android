package com.liu.android.gitee.ui.main;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.liu.android.gitee.R;
import com.liu.android.gitee.app.Constant;
import com.liu.android.gitee.base.BaseFragment;
import com.liu.android.gitee.databinding.FragmentSettingsBinding;
import com.liu.android.gitee.service.UpdateService;
import com.liu.android.gitee.ui.fileupdown.FileUpDownActivity;
import com.liu.android.gitee.ui.login.LoginActivity;
import com.liu.android.gitee.utils.DialogUtil;
import com.liu.android.gitee.utils.SPUtil;

import java.util.Random;

import static com.liu.android.gitee.utils.CommonUtil.toast;

/**
 * 设置页面
 */
public class SettingsFragment extends BaseFragment {

    private FragmentSettingsBinding binding;

    @Override
    protected View initContentView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        binding = FragmentSettingsBinding.inflate(inflater, container, false);
        return binding.getRoot();
    }

    @Override
    protected void initListener() {
        binding.fileUpDown.setOnClickListener(v -> {
            Intent intent = new Intent(mContext, FileUpDownActivity.class);
            startActivity(intent);
        });
        binding.vCheckUpdate.setOnClickListener(v -> {
            //检查升级
            boolean needUpdate = new Random().nextBoolean();
            if (!needUpdate) {
                toast(getString(R.string.isTheLatestVersion));
                return;
            }
            SPUtil.getInstance().put(Constant.APK_URL, FileUpDownActivity.url);
            SPUtil.getInstance().put(Constant.APK_NAME, getString(R.string.app_name));
            DialogUtil alterDialogUtils = new DialogUtil(mContext);
            alterDialogUtils.setTitle(getString(R.string.newVersion));
            alterDialogUtils.setMessage(getString(R.string.okAndroid11));
            alterDialogUtils.setTwoConfirmBtn(getString(R.string.updateNow), v1 -> {
                toast(getString(R.string.downloadingInBackground));
                mContext.startService(new Intent(mContext, UpdateService.class));
            });
            alterDialogUtils.setTwoCancelBtn(getString(R.string.nextReminder), v2 -> {
            });
            alterDialogUtils.setCancelable(false);
            alterDialogUtils.show();
        });
        binding.vLogout.setOnClickListener(v -> {
            SPUtil.getInstance().clear();
            startActivity(new Intent(mContext, LoginActivity.class));
            mContext.finish();
        });
    }

    @Override
    protected void initData() {
    }

}
