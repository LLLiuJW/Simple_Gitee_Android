package com.liu.android.gitee.net;


import com.liu.android.gitee.base.BaseResultEntity;
import com.liu.android.gitee.bean.CommitEntity;
import com.liu.android.gitee.bean.ReposEntity;
import com.liu.android.gitee.bean.UserInfoEntity;

import java.util.List;

import io.reactivex.Observable;
import okhttp3.MultipartBody;
import okhttp3.ResponseBody;
import retrofit2.http.GET;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.Path;
import retrofit2.http.Query;
import retrofit2.http.Streaming;
import retrofit2.http.Url;

/**
 * APIservice
 * 封装api请求
 */
public interface ApiService {

    //获取我的信息
    @GET("user")
    Observable<UserInfoEntity> getMyInfo(@Query("access_token") String token);

    //获取用户信息
    @GET("users/{userName}")
    Observable<UserInfoEntity> getUserInfo(@Path("userName") String userName);

    //获取用户仓库
    @GET("users/{userName}/repos")
    Observable<List<ReposEntity>> getMyRepos(@Path("userName") String userName, @Query("per_page") int per_page, @Query("page") long page);

    //获取星标仓库
    @GET("users/{userName}/starred")
    Observable<List<ReposEntity>> getStarred(@Path("userName") String userName, @Query("per_page") int per_page, @Query("page") long page);

    //获取被追随
    @GET("users/{userName}/followers")
    Observable<List<UserInfoEntity>> getFollowers(@Path("userName") String userName, @Query("per_page") int per_page, @Query("page") long page);

    //获取仓库信息
    @GET("repos/{userName}/{reposName}")
    Observable<ReposEntity> getReposInfo(@Path("userName") String userName,
                                         @Path("reposName") String reposName);

    //获取仓库提交记录 分页
    @GET("repos/{userName}/{reposName}/commits?sha=master")
    Observable<List<CommitEntity>> getCommits(@Path("userName") String userName,
                                              @Path("reposName") String reposName,
                                              @Query("per_page") int per_page, @Query("page") long page);

    //文件下载
    @Streaming/*大文件需要加入这个判断，防止下载过程中写入到内存中*/
    @GET
    Observable<ResponseBody> download(@Url String url);

    //文件上传 模拟
    @Multipart
    @POST("api/user/update")
    Observable<BaseResultEntity> upload(@Part MultipartBody.Part part);

}
