package com.liu.android.gitee.ui.repos;


import com.liu.android.gitee.app.Constant;
import com.liu.android.gitee.base.BaseViewModel;
import com.liu.android.gitee.bean.ReposEntity;
import com.liu.android.gitee.net.HttpListener;
import com.liu.android.gitee.repository.MyModel;
import com.liu.android.gitee.utils.SPUtil;

import java.util.ArrayList;
import java.util.List;

import androidx.lifecycle.MutableLiveData;

/**
 * ReposViewModel
 */
public class ReposViewModel extends BaseViewModel {

    private final int pageSize = 10;//分页
    public MutableLiveData<String> myReposResultCode = new MutableLiveData<>();//0成功；other失败
    public String myReposResultMsg;
    public boolean myReposRefresh;//true 下拉刷新；false 上拉加载
    public boolean myReposNewEmpty;//true 增量为空；false 增量不为空
    public List<ReposEntity> myReposData = new ArrayList<>();//列表数据
    public MutableLiveData<String> starredResultCode = new MutableLiveData<>();//0成功；other失败
    public String starredResultMsg;
    public boolean starredRefresh;//true 下拉刷新；false 上拉加载
    public boolean starredNewEmpty;//true 增量为空；false 增量不为空
    public List<ReposEntity> starredData = new ArrayList<>();//列表数据
    //MyRepos
    private long myReposPageIndex;//索引
    //Starred
    private long starredPageIndex;//索引

    public ReposViewModel() {
        mModel = new MyModel();
    }

    /**
     * 获取列表
     *
     * @param isRefresh true 下拉刷新；false 上拉加载
     */
    public void getMyRepos(boolean isRefresh) {
        if (isRefresh) {
            myReposPageIndex = 1;
        }
        String userName = SPUtil.getInstance().getString(Constant.USER_INFO);
        ((MyModel) mModel).getMyRepos(userName, pageSize, myReposPageIndex, new HttpListener() {
            @Override
            public void onSuccess(Object object) {
                List<ReposEntity> list = (List<ReposEntity>) object;
                myReposPageIndex++;//偏移量+1
                if (isRefresh) //下拉覆盖，上拉增量
                    myReposData.clear();
                if (!list.isEmpty())
                    myReposData.addAll(list);
                myReposRefresh = isRefresh;
                myReposNewEmpty = list.isEmpty();
                myReposResultCode.postValue("0");
            }

            @Override
            public void onError(String errorMsg, String code) {
                myReposResultMsg = errorMsg;
                myReposRefresh = isRefresh;
                myReposResultCode.postValue(code);
            }
        });
    }

    /**
     * 获取列表
     *
     * @param isRefresh true 下拉刷新；false 上拉加载
     */
    public void getStarred(boolean isRefresh) {
        if (isRefresh) {
            starredPageIndex = 1;
        }
        String userName = SPUtil.getInstance().getString(Constant.USER_INFO);
        ((MyModel) mModel).getStarred(userName, pageSize, starredPageIndex, new HttpListener() {
            @Override
            public void onSuccess(Object object) {
                List<ReposEntity> list = (List<ReposEntity>) object;
                starredPageIndex++;//偏移量+1
                if (isRefresh) //下拉覆盖，上拉增量
                    starredData.clear();
                if (!list.isEmpty())
                    starredData.addAll(list);
                starredRefresh = isRefresh;
                starredNewEmpty = list.isEmpty();
                starredResultCode.postValue("0");
            }

            @Override
            public void onError(String errorMsg, String code) {
                starredResultMsg = errorMsg;
                starredRefresh = isRefresh;
                starredResultCode.postValue(code);
            }
        });
    }
}
