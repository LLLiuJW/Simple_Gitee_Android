package com.liu.android.gitee.net;


import com.liu.android.gitee.app.Constant;
import com.liu.android.gitee.net.download.DownloadInterceptor;
import com.liu.android.gitee.net.download.DownloadListener;

import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Retrofit封装类
 */
public class RetrofitClient {
    private final static int connectTimeout = 6;//s,连接超时
    private final static int readTimeout = 6;//s,读取超时
    private final static int writeTimeout = 6;//s,写超时
    private volatile static RetrofitClient INSTANCE;
    private AuthService authService;//auth服务器
    private ApiService apiService;//api服务器
    private ApiService downloadService;//下载服务器

    //构造方法私有
    private RetrofitClient() {
    }

    //获取单例
    public static RetrofitClient getInstance() {
        if (INSTANCE == null) {
            synchronized (RetrofitClient.class) {
                if (INSTANCE == null) {
                    INSTANCE = new RetrofitClient();
                }
            }
        }
        return INSTANCE;
    }

    //auth服务器
    public AuthService getAuthService() {
        if (authService == null) {
            OkHttpClient okHttpClient = new OkHttpClient.Builder()
                    .connectTimeout(connectTimeout, TimeUnit.SECONDS)
                    .readTimeout(readTimeout, TimeUnit.SECONDS)
                    .writeTimeout(writeTimeout, TimeUnit.SECONDS)
                    .retryOnConnectionFailure(false)//出现错误时会重新发送请求
                    .addInterceptor(new LogInterceptor())//日志拦截器
                    .build();
            Retrofit retrofit = new Retrofit.Builder()
                    .client(okHttpClient)
                    .addConverterFactory(GsonConverterFactory.create())
                    .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                    .baseUrl(Constant.baseUrl)
                    .build();
            authService = retrofit.create(AuthService.class);
        }
        return authService;
    }

    //api服务器
    public ApiService getApiService() {
        if (apiService == null) {
            OkHttpClient okHttpClient = new OkHttpClient.Builder()
                    .connectTimeout(connectTimeout, TimeUnit.SECONDS)
                    .readTimeout(readTimeout, TimeUnit.SECONDS)
                    .writeTimeout(writeTimeout, TimeUnit.SECONDS)
                    .retryOnConnectionFailure(false)//出现错误时会重新发送请求
                    .addInterceptor(new LogInterceptor())//日志拦截器
                    .addInterceptor(new TokenHeaderInterceptor())//token拦截器
                    .build();
            Retrofit retrofit = new Retrofit.Builder()
                    .client(okHttpClient)
                    .addConverterFactory(GsonConverterFactory.create())
                    .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                    .baseUrl(Constant.baseUrl)
                    .build();
            apiService = retrofit.create(ApiService.class);
        }
        return apiService;
    }

    //下载服务器
    public ApiService getDownloadService(DownloadListener listener) {
        if (downloadService == null) {
            OkHttpClient.Builder builder = new OkHttpClient.Builder();
            builder.connectTimeout(connectTimeout, TimeUnit.SECONDS);
            builder.readTimeout(readTimeout, TimeUnit.SECONDS);
            builder.writeTimeout(writeTimeout, TimeUnit.SECONDS);
            builder.retryOnConnectionFailure(false);//出现错误时会重新发送请求
            if (listener != null)
                builder.addInterceptor(new DownloadInterceptor(listener));//下载拦截器（显示进度）
            Retrofit retrofit = new Retrofit.Builder()
                    .client(builder.build())
                    .addConverterFactory(GsonConverterFactory.create())
                    .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                    .baseUrl(Constant.downloadHost)
                    .build();
            downloadService = retrofit.create(ApiService.class);
        }
        return downloadService;
    }
}