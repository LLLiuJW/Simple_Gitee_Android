package com.liu.android.gitee.repository;


import com.liu.android.gitee.app.Constant;
import com.liu.android.gitee.base.BaseModel;
import com.liu.android.gitee.utils.CreateRequestBodyUtil;
import com.liu.android.gitee.net.HttpListener;
import com.liu.android.gitee.net.RetrofitClient;
import com.liu.android.gitee.net.download.DownloadListener;
import com.liu.android.gitee.utils.SPUtil;

import java.io.File;

import androidx.annotation.Nullable;
import okhttp3.MultipartBody;

/**
 * MyModel
 * 数据层，负责数据获取
 */
public class MyModel extends BaseModel {
    //登陆接口
    public void login(String userName, String passWord, HttpListener listener) {
        sendRequest(RetrofitClient.getInstance().getAuthService().getToken(Constant.LOGIN_TYPE,
                userName,
                passWord,
                Constant.CLIENT_ID,
                Constant.CLIENT_SECRET,
                Constant.PERMISSION_SCOPE),
                listener);
    }

    public void refreshToken(String refresh_token, HttpListener listener) {
        sendRequest(RetrofitClient.getInstance().getAuthService().refreshToken(Constant.REFRESH_TYPE,refresh_token), listener);
    }

    public void getMyInfo(String token, HttpListener listener){
        sendRequest(RetrofitClient.getInstance().getApiService().getMyInfo(token), listener);
    }

    public void getUserInfo(String userName, HttpListener listener) {
        sendRequest(RetrofitClient.getInstance().getApiService().getUserInfo(userName), listener);
    }

    public void getFollowers(String userName, int pageSize, long pageIndex, HttpListener listener) {
        sendRequest(RetrofitClient.getInstance().getApiService().getFollowers(userName, pageSize, pageIndex),listener);
    }

    public void getMyRepos(String username, int pageSize, long pageIndex, HttpListener listener) {
        sendRequest(RetrofitClient.getInstance().getApiService().getMyRepos(username,pageSize, pageIndex), listener);
    }

    public void getStarred(String userName, int pageSize, long pageIndex, HttpListener listener) {
        sendRequest(RetrofitClient.getInstance().getApiService().getStarred(userName, pageSize, pageIndex), listener);
    }

    public void getReposInfo(String userName, String reposName, HttpListener listener) {
        sendRequest(RetrofitClient.getInstance().getApiService().getReposInfo(userName, reposName), listener);
    }

    public void getCommits(String userName, String reposName, int pageSize, long pageIndex, HttpListener listener) {
        sendRequest(RetrofitClient.getInstance().getApiService().getCommits(userName, reposName, pageSize, pageIndex), listener);
    }

    /**
     * 下载文件
     *
     * @param url      地址
     * @param file     位置
     * @param listener
     */
    public void download(String url, File file, DownloadListener listener) {
        sendDownloadRequest(RetrofitClient.getInstance().getDownloadService(listener).download(url), new HttpListener() {
            @Override
            public void onSuccess(Object object) {
                listener.onSuccess(object);
            }

            @Override
            public void onError(String errorMsg, String code) {
                listener.onError(errorMsg, code);
            }
        }, file);
    }

    //文件上传
    public void upload(String key, File file, HttpListener listener) {
        MultipartBody.Part part = CreateRequestBodyUtil.createRequestBody(key, file);
        sendRequest(RetrofitClient.getInstance().getApiService().upload(part), listener);
    }
}
