package com.liu.android.gitee.app;

import android.app.Activity;
import android.app.Application;
import android.os.Bundle;

import com.liu.android.gitee.utils.CommonUtil;
import com.liu.android.gitee.utils.Density;

import androidx.annotation.NonNull;

/**
 * MyAPP
 */
public class App extends Application {
    //获取全局上下文  CommonUtil.getContext();
    //private static Application sInstance;

    public static synchronized void setApplication(@NonNull Application application) {
        //sInstance = application;
        //注册监听每个activity的生命周期,便于堆栈式管理
        application.registerActivityLifecycleCallbacks(new ActivityLifecycleCallbacks() {

            @Override
            public void onActivityCreated(Activity activity, Bundle savedInstanceState) {
                AppManager.getInstance().addActivity(activity);
            }

            @Override
            public void onActivityStarted(Activity activity) {
            }

            @Override
            public void onActivityResumed(Activity activity) {
            }

            @Override
            public void onActivityPaused(Activity activity) {
            }

            @Override
            public void onActivityStopped(Activity activity) {
            }

            @Override
            public void onActivitySaveInstanceState(Activity activity, Bundle outState) {
            }

            @Override
            public void onActivityDestroyed(Activity activity) {
                AppManager.getInstance().removeActivity(activity);
            }
        });
    }

    @Override
    public void onCreate() {
        super.onCreate();
        setApplication(this);
        //初始化工具类
        CommonUtil.init(this);
        //屏幕适配方案，根据ui图修改,屏幕最小宽度375dp
        Density.setDensity(this, 375f);
        //异常捕获后重启，umeng等可能无法统计到异常信息
//        CrashHandler.getInstance().init(this);
    }

}
