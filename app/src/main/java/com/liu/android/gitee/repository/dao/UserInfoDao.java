package com.liu.android.gitee.repository.dao;


import com.liu.android.gitee.bean.UserInfoEntity;

import java.util.List;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;

/**
 * 用户信息Dao
 */
@Dao
public interface UserInfoDao {
    @Query("select * from user_info")
    LiveData<List<UserInfoEntity>> loadAll();

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insertAll(List<UserInfoEntity> entity);

    @Query("delete from user_info")
    void deleteAll();

    @Query("SELECT MAX(indexInResponse) + 1 FROM user_info")
    int getNextIndex();
}
