package com.liu.android.gitee.net.download;

/**
 * 用于下载时的接口回调
 */
public interface DownloadListener {
    void onSuccess(Object object);

    void onError(String errorMsg, String code);

    void update(long read, long count);//已下载，总量
}
