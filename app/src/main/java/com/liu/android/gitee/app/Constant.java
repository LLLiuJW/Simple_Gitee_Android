package com.liu.android.gitee.app;


import com.liu.android.gitee.utils.SPUtil;

import java.util.HashMap;

/**
 * 全局常量类
 */
public class Constant {
    //SP Key
    public static final String USER_INFO = "USER_INFO";//用户名
    public static final String APK_URL = "APK_URL";//apk下载地址
    public static final String APK_NAME = "Simple_Gitee_Android";//apk文件名称
    public static final String KEY_REMEMBER_ME = "REMEMBER_ME";//记住我
    public static final String KEY_AUTO_LOGIN = "AUTO_LOGIN";//自动登录
    public static final String KEY_ACCOUNT = "ACCOUNT";//账号
    public static final String KEY_PASSWORD = "PASSWORD";//密码
    public static final String APP_REPOS_NAME = "Simple_Gitee_Android";//该项目APP的git仓库名称
    public static final String TOKEN = "access_token";//token
    public static final String LOGIN_TYPE = "password";//gitee密码登录时的grant_type
    public static final String REFRESH_TYPE = "refresh_token";//gitee刷新令牌时的grant_type
    public static final String PERMISSION_SCOPE = "user_info projects pull_requests issues notes groups gists enterprises";//gitee 第三方应用的权限范围scope
    public static final String CLIENT_ID = "XXXXX";//gitee client_id
    public static final String CLIENT_SECRET = "XXXXXX";//gitee client_secret
    public static final long TOKEN_EXPIRE_INTERVAL = 86400000;//token过期时间，一天
    public static final long REFRESH_TIME = 3600000;//重新刷新快过期token的时间，一小时
    public static String baseUrl = "https://gitee.com/api/v5/"; //API服务器
    public static String downloadHost = "http://cps.yingyonghui.com/"; //下载线路


    //请求通用参数
    public static HashMap<String, String> getBaseMap() {
        String token = SPUtil.getInstance().getString(TOKEN);
        HashMap<String, String> map = new HashMap<>();
        map.put("access_token", token);
        return map;
    }

}
