package com.liu.android.gitee.ui.repos;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.liu.android.gitee.adapter.MyRecyclerAdapters;
import com.liu.android.gitee.base.BaseFragment;
import com.liu.android.gitee.bean.ReposEntity;
import com.liu.android.gitee.databinding.FragmentSwipeListBinding;
import com.liu.android.gitee.utils.CommonUtil;

import androidx.lifecycle.ViewModelProvider;

/**
 * 关注页面
 */
public class StarredFragment extends BaseFragment {

    protected boolean hasRequested;//在onResume中判断是否已经请求过数据。用于懒加载
    private FragmentSwipeListBinding binding;
    private ReposViewModel viewModel;
    private MyRecyclerAdapters.ReposRecyclerAdapter recyclerAdapter;

    @Override
    protected View initContentView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        binding = FragmentSwipeListBinding.inflate(inflater, container, false);
        return binding.getRoot();
    }

    @Override
    protected void initListener() {
        binding.refreshLayout.setOnRefreshListener(v -> viewModel.getStarred(true));
        binding.refreshLayout.setOnLoadMoreListener(v -> viewModel.getStarred(false));
    }

    @Override
    protected void initData() {
        viewModel = new ViewModelProvider(requireActivity()).get(ReposViewModel.class);

        recyclerAdapter = new MyRecyclerAdapters.ReposRecyclerAdapter(mContext, viewModel.starredData);
        recyclerAdapter.setOnItemClickListener((v, position) -> {
            ReposEntity data = viewModel.starredData.get(position);
            startActivity(ReposDetailActivity.getStartIntent(mContext, data.getName(), data.getOwner().getLogin()));
        });
        binding.recyclerView.setAdapter(recyclerAdapter);
    }

    @Override
    protected void initViewObservable() {
        viewModel.starredResultCode.observe(this, code -> {
            if (code.equals("0")) {
                recyclerAdapter.notifyDataSetChanged();
                if (viewModel.starredRefresh) {
                    if (viewModel.starredNewEmpty) {
                        binding.refreshLayout.finishRefreshWithNoMoreData();//上拉加载功能将显示没有更多数据
                    } else {
                        binding.refreshLayout.finishRefresh();
                    }
                } else {
                    if (viewModel.starredNewEmpty) {
                        binding.refreshLayout.finishLoadMoreWithNoMoreData();//上拉加载功能将显示没有更多数据
                    } else {
                        binding.refreshLayout.finishLoadMore();
                    }
                }
            } else {
                CommonUtil.toast(viewModel.starredResultMsg);
                if (viewModel.starredRefresh) {
                    binding.refreshLayout.finishRefresh(false);//刷新失败，会影响到上次的更新时间
                } else {
                    binding.refreshLayout.finishLoadMore(false);
                }
            }
            binding.emptyLayout.llEmpty.setVisibility(viewModel.starredData.isEmpty() ? View.VISIBLE : View.GONE);
        });
    }

    @Override
    public void onResume() {
        super.onResume();
        if (!hasRequested) {
            hasRequested = true;
            binding.refreshLayout.autoRefresh();//触发自动刷新
        }
    }

    @Override
    public void onDestroy() {
        if (viewModel != null) {
            viewModel.dispose();
            viewModel = null;
        }
        super.onDestroy();
    }
}