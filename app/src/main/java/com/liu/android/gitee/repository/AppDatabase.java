package com.liu.android.gitee.repository;


import com.liu.android.gitee.bean.UserInfoEntity;
import com.liu.android.gitee.repository.dao.UserInfoDao;
import com.liu.android.gitee.utils.CommonUtil;

import androidx.room.Database;
import androidx.room.Room;
import androidx.room.RoomDatabase;

/**
 * SQLite数据库类
 * 存储用户信息
 */
@Database(entities = {UserInfoEntity.class}, version = 1, exportSchema = false)
public abstract class AppDatabase extends RoomDatabase {

    public abstract UserInfoDao userInfoDao();

    private static class AppDataBaseHolder {
        private static final AppDatabase instance =
                Room.databaseBuilder(CommonUtil.getContext(), AppDatabase.class, "basic.db")
                        .fallbackToDestructiveMigration()//升级时丢弃原来表
                        .build();
    }

    public static AppDatabase getInstance() {
        return AppDataBaseHolder.instance;
    }


    //注意，onDestroy时关闭数据库 close();
}
