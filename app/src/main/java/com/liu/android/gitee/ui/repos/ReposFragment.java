package com.liu.android.gitee.ui.repos;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.liu.android.gitee.R;
import com.liu.android.gitee.adapter.MyFragmentPagerAdapter;
import com.liu.android.gitee.base.BaseFragment;
import com.liu.android.gitee.databinding.FragmentReposBinding;

import java.util.ArrayList;
import java.util.List;

/**
 * 仓库页面
 */
public class ReposFragment extends BaseFragment {

    private FragmentReposBinding binding;

    @Override
    public View initContentView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        binding = FragmentReposBinding.inflate(inflater, container, false);
        return binding.getRoot();
    }

    @Override
    protected void initListener() {
    }

    @Override
    protected void initData() {
        List<BaseFragment> fragmentList = new ArrayList<>();
        fragmentList.add(new MyReposFragment());
        fragmentList.add(new StarredFragment());

        List<String> titles = new ArrayList<>();
        titles.add(getString(R.string.myRepos));
        titles.add(getString(R.string.starred));

        MyFragmentPagerAdapter pagerAdapter = new MyFragmentPagerAdapter(getChildFragmentManager());
        pagerAdapter.setData(fragmentList, titles);
        binding.viewPager.setAdapter(pagerAdapter);
        binding.viewPager.setOffscreenPageLimit(fragmentList.size());
        binding.tabLayout.setupWithViewPager(binding.viewPager);
    }
}