package com.liu.android.gitee.ui.main;

import android.os.Bundle;
import android.view.View;

import com.liu.android.gitee.adapter.MyFragmentPagerAdapter;
import com.liu.android.gitee.base.BaseActivity;
import com.liu.android.gitee.base.BaseFragment;
import com.liu.android.gitee.databinding.ActivityMainBinding;
import com.liu.android.gitee.ui.follow.FollowersFragment;
import com.liu.android.gitee.ui.repos.ReposFragment;

import java.util.ArrayList;
import java.util.List;

import androidx.viewpager.widget.ViewPager;

/**
 * 主界面
 */
public class MainActivity extends BaseActivity {

    private ActivityMainBinding binding;

    @Override
    public View initContentView(Bundle savedInstanceState) {
        binding = ActivityMainBinding.inflate(getLayoutInflater());
        return binding.getRoot();
    }

    @Override
    protected void initListener() {
    }

    @Override
    protected void initData() {

        List<BaseFragment> fragmentList = new ArrayList<>();
        fragmentList.add(new ReposFragment());
        fragmentList.add(new FollowersFragment());
        fragmentList.add(new SettingsFragment());
        fragmentList.add(new AboutFragment());

        MyFragmentPagerAdapter pagerAdapter = new MyFragmentPagerAdapter(getSupportFragmentManager());
        pagerAdapter.setData(fragmentList, null);
        binding.viewPager.setAdapter(pagerAdapter);
        binding.viewPager.setOffscreenPageLimit(3);//缓存当前界面每一侧的界面数
        binding.viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
            }

            @Override
            public void onPageSelected(int position) {
                binding.navigation.getMenu().getItem(position).setChecked(true);
            }

            @Override
            public void onPageScrollStateChanged(int state) {
            }
        });
        binding.navigation.setOnNavigationItemSelectedListener(item -> {
            binding.viewPager.setCurrentItem(item.getOrder());
            return true;
        });
    }
}
