package com.liu.android.gitee.base;

import androidx.lifecycle.ViewModel;

/**
 * ViewModel基类
 */
public class BaseViewModel extends ViewModel {
    protected BaseModel mModel;

    /**
     * 解除model中所有订阅者
     */
    public void dispose() {
        if (mModel != null) {
            mModel.dispose();
            mModel = null;
        }
    }
}
