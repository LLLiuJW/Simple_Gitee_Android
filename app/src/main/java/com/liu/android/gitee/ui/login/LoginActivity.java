package com.liu.android.gitee.ui.login;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.CompoundButton;

import com.liu.android.gitee.R;
import com.liu.android.gitee.app.Constant;
import com.liu.android.gitee.base.BaseActivity;
import com.liu.android.gitee.bean.TokenEntity;
import com.liu.android.gitee.databinding.ActivityLoginBinding;
import com.liu.android.gitee.ui.main.MainActivity;
import com.liu.android.gitee.utils.CommonUtil;
import com.liu.android.gitee.utils.LogUtil;
import com.liu.android.gitee.utils.SPUtil;

import java.util.Objects;

import androidx.lifecycle.ViewModelProvider;

/**
 * 登录页面
 */
public class LoginActivity extends BaseActivity {

    private ActivityLoginBinding binding;
    private LoginViewModel viewModel;

    @Override
    protected View initContentView(Bundle savedInstanceState) {
        binding = ActivityLoginBinding.inflate(getLayoutInflater());
        binding.setLifecycleOwner(this);
        return binding.getRoot();
    }

    @Override
    protected void initListener() {
        binding.etPwd.setOnEditorActionListener((v, actionId, event) -> {
            if (actionId == EditorInfo.IME_ACTION_GO) {
                loginRequest();
                return true;
            }
            return false;
        });
        binding.btnLogin.setOnClickListener(v -> loginRequest());
//        binding.rememberMeBox.setOnCheckedChangeListener((compoundButton, b) -> viewModel.rememberMe.setValue(compoundButton.isChecked()));
//        binding.autoLoginBox.setOnCheckedChangeListener((compoundButton, b) -> viewModel.autoLogin.setValue(compoundButton.isChecked()));
    }

    @Override
    protected void initData() {
        viewModel = new ViewModelProvider(this).get(LoginViewModel.class);
        binding.setViewModel(viewModel);
    }

    @Override
    protected void initViewObservable() {
        viewModel.loginResultCode.observe(this, code -> {
            if (code.equals("0")) {
                if (viewModel.loginResultData != null) {
                    CommonUtil.toast(getString(R.string.loginSuccess));
                }
                viewModel.getMyInfo();
                startActivity(new Intent(mContext, MainActivity.class));
                finish();
            } else {
                if (!TextUtils.isEmpty(viewModel.loginResultMsg)) {
                    CommonUtil.toast(viewModel.loginResultMsg);
                }
                binding.btnLogin.setEnabled(true);
            }
        });
        viewModel.isLoading.observe(this, b -> {
            if (b) {
                showProgressDialog(getString(R.string.loading));
            } else {
                dismissProgressDialog();
            }
        });
        viewModel.rememberMe.observe(this,b -> {
            if (b) {
                String account = viewModel.userName.getValue() != null? viewModel.userName.getValue():"";
                String password = viewModel.password.getValue() != null? viewModel.password.getValue():"";
                SPUtil.getInstance().put(Constant.KEY_ACCOUNT, account);
                SPUtil.getInstance().put(Constant.KEY_PASSWORD, password);
            }
            SPUtil.getInstance().put(Constant.KEY_REMEMBER_ME,b);
        });
        viewModel.autoLogin.observe(this,b -> {
            if(b){
                viewModel.rememberMe.setValue(true);
            }
            SPUtil.getInstance().put(Constant.KEY_AUTO_LOGIN,b);
        });
    }

    @Override
    protected void onDestroy() {
        if (viewModel != null) {
            viewModel.dispose();
            viewModel = null;
        }
        super.onDestroy();
    }

    /**
     * 登录请求
     */
    private void loginRequest() {
        boolean valid = true;
        if (TextUtils.isEmpty(viewModel.userName.getValue())) {
            valid = false;
            binding.llName.setError(getString(R.string.user_name_warning));
        } else {
            binding.llName.setErrorEnabled(false);
        }
        if (TextUtils.isEmpty(viewModel.password.getValue())) {
            valid = false;
            binding.llPwd.setError(getString(R.string.password_warning));
        } else {
            binding.llPwd.setErrorEnabled(false);
        }
        if (valid) {
            binding.btnLogin.setEnabled(false);
            viewModel.login();
        }
    }
}
