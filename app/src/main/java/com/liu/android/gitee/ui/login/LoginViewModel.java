package com.liu.android.gitee.ui.login;

import android.text.TextUtils;

import com.google.gson.Gson;
import com.liu.android.gitee.app.Constant;
import com.liu.android.gitee.base.BaseViewModel;
import com.liu.android.gitee.bean.TokenEntity;
import com.liu.android.gitee.bean.UserInfoEntity;
import com.liu.android.gitee.net.HttpListener;
import com.liu.android.gitee.repository.MyModel;
import com.liu.android.gitee.utils.CommonUtil;
import com.liu.android.gitee.utils.SPUtil;

import androidx.lifecycle.MutableLiveData;

/**
 * LoginViewModel
 */
public class LoginViewModel extends BaseViewModel {

    public MutableLiveData<Boolean> isLoading = new MutableLiveData<>();
    public MutableLiveData<Boolean> rememberMe = new MutableLiveData<>();
    public MutableLiveData<Boolean> autoLogin = new MutableLiveData<>();
    public MutableLiveData<String> userName = new MutableLiveData<>();
    public MutableLiveData<String> password = new MutableLiveData<>();
    //登录
    public MutableLiveData<String> loginResultCode = new MutableLiveData<>();//0成功；other失败
    public String loginResultMsg;
    public TokenEntity loginResultData;

    public LoginViewModel() {
        mModel = new MyModel();
        init();
    }

    @SuppressWarnings("ConstantConditions")
    private void init() {
        SPUtil sp = SPUtil.getInstance();
        rememberMe.setValue(sp.getBoolean(Constant.KEY_REMEMBER_ME,false));
        autoLogin.setValue(sp.getBoolean(Constant.KEY_AUTO_LOGIN,false));
        if(rememberMe.getValue()){
            userName.setValue(sp.getString(Constant.KEY_ACCOUNT,""));
            password.setValue(sp.getString(Constant.KEY_PASSWORD,""));
        }
        if (autoLogin.getValue()){
            login();
        }
    }

    public void login() {
        isLoading.setValue(true);
        ((MyModel) mModel).login(userName.getValue(), password.getValue(), new HttpListener() {
            @Override
            public void onSuccess(Object object) {
                isLoading.postValue(false);
                loginResultData = (TokenEntity) object;
                SPUtil.getInstance().put(Constant.TOKEN, new Gson().toJson(object));
//                SPUtil.getInstance().put(Constant.USER_INFO, "LLLiuJW");
                loginResultCode.postValue("0");
            }

            @Override
            public void onError(String errorMsg, String code) {
                isLoading.postValue(false);
                loginResultMsg = errorMsg;
                loginResultCode.postValue(code);
            }
        });
    }

    public void getMyInfo(){
        String token = (new Gson().fromJson(SPUtil.getInstance().getString(Constant.TOKEN),TokenEntity.class)).getAccess_token();
        ((MyModel)mModel).getMyInfo(token, new HttpListener() {
            @Override
            public void onSuccess(Object object) {
                String username = ((UserInfoEntity)object).getLogin();
                SPUtil.getInstance().put(Constant.USER_INFO, username);
            }

            @Override
            public void onError(String errorMsg, String code) {
                CommonUtil.toast(errorMsg);
            }
        });
    }
}
